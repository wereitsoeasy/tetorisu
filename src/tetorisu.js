class tetrimino {
  // vertices are the set of points that describe the tetrimino shape
  
  constructor (ctx, vertices, vy){
    this.ctx = ctx;
    this.is_colliding = false;
    this.vertices = vertices;
    this.vy = vy;
  }

  draw(){
    // draw the tetrimino
    this.ctx.beginPath();
    var i;
    for( i = 0; i < this.vertices.length; i++ ){
      this.ctx.moveTo(this.vertices[i][0], this.vertices[i][1]);
      if(i != (this.vertices.length - 1)){
        this.ctx.lineTo(this.vertices[i+1][0], this.vertices[i+1][1])
      }
    }
    this.ctx.stroke();
  }
}

function draw_canvas(){
  var canvas = document.createElement("canvas");
  canvas.innerHTML = "canvas not supported by browser";
  canvas.height = "400";
  canvas.width = "200";
  if (canvas.getContext) {
    var ctx = canvas.getContext("2d");
  }
  ctx.font = "20px serif";
  ctx.fillStyle = "#FFFFFF";
  ctx.strokeStyle = "#FFFFFF";
  ctx.textBaseline = "ideographic";
  vert = [[0,0], [80,0], [80,20], [0,20]];
  cyan = new tetrimino(ctx, vert);
  cyan.draw();

  document.getElementById("div-canvas").appendChild(canvas);
}

draw_canvas();


//text mode
var grid = new Array(20);

function init_grid(){
  var i, j;
  for (i = 0; i < 20; i++) {
    grid[i] = new Array(10);
    for (j = 0; j < 10; j++){
      grid[i][j] = 0;
    }
  }
}

function print_grid(){
  var pre = document.createElement("pre");
  grid.forEach(val => pre.innerHTML += (val + "\n"));
  //document.getElementById("div-debug").appendChild(pre);
}


